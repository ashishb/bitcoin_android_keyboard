package com.anysoftkeyboard.ui;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.coinbase.api.Coinbase;
import com.coinbase.api.CoinbaseBuilder;
import com.coinbase.api.exception.CoinbaseException;
import com.menny.android.anysoftkeyboard.R;
import com.anysoftkeyboard.CoinBaseApiKeys;

import java.io.IOException;
import java.util.List;

import android.os.AsyncTask;

public class BitcoinHandlerActivity extends ActionBarActivity {

    private static final String TAG1 = "ASHISH";
    private Coinbase cb;
    private String emailAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bitcoin_handler);
        new Thread() {
            @Override
            public void run() {
                initCb();
            }
        }.start();
        Intent intent = getIntent();
        String action = intent.getAction();
        Log.d(TAG1, intent.toString());
        Log.d(TAG1, action);
        List<String> segments = intent.getData().getPathSegments();
        if (segments.size() == 2) {
            final String tokenId = segments.get(0);
            String money = segments.get(1);
            String mainText = String.format("Received %s $ worth of Bitcoins.", money);
            final TextView mainBox = (TextView) findViewById(R.id.received_money_mainbox);
            mainBox.setText(mainText);
            AsyncTask<Void, Void, Boolean> task = new AsyncTask<Void, Void, Boolean>() {
                @Override
                protected Boolean doInBackground(Void[] params) {
                    initCb();
                    try {
                        cb.redeemToken(tokenId);
                        return true;
                    } catch (CoinbaseException | IOException e) {
                        Log.e(TAG1, "coin base exception", e);
                    }
                    return false;
                }

                @Override
                protected void onPostExecute(Boolean result) {
                    if (result) {
                        mainBox.setText(mainBox.getText() + "\nSuccessfully deposited money to " + emailAddress);
                        // Finish the activity after sometime.
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                finish();
                            }
                        }, 5 * 1000);
                    } else {
                        mainBox.setText(mainBox.getText() + "\nFailed to deposit money to " + emailAddress + "\nMay be someone has already claimed the money?");
                    }
                }
            };
            task.execute();
        } else {
            Log.e(TAG1, "unexpected data, ignoring.");
            for (String segment : intent.getData().getPathSegments()) {
                Log.e(TAG1, "segment: " + segment);
            }
        }
    }

    private void initCb() {
        if (cb == null) {
            cb = new CoinbaseBuilder()
                    .withApiKey(CoinBaseApiKeys.API_KEY, CoinBaseApiKeys.API_SECRET)
                    .build();
            try {
                emailAddress = cb.getUser().getEmail();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (CoinbaseException e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_bitcoin_handler, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
