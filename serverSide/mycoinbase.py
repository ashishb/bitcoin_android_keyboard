from coinbase.client import Client

import api

client = Client(api.API_KEY, api.API_SECRET)


def transferMoney(tokenId, emailAddress):
    # Step 1: Redeem token to our account
    print 'Going to redeem the token first'
    amount = redeemToken(tokenId)
    if amount <= 0:
        return False
    print 'Token redeemded, now transferring ', amount, ' to ', emailAddress

    # Step 2: Send money to email Address.
    return sendMoney(amount, emailAddress)


def redeemToken(tokenId):
    if not client.redeem_token(tokenId):
        return -1
    account = client.get_account()
    transactions = account.get_transactions()
    transaction = transactions.transactions[0]
    amount_in_btc = transaction.amount.amount
    print "amount in bitcoins: " + amount_in_btc
    return amount_in_btc


def sendMoney(amount, emailAddress):
    wallet = client.get_account()
    print "Sending " + amount + " in BTC " + " to " + emailAddress
    wallet.send_money(emailAddress, amount, notes='Via Bitcoin Keyboard')
    return True

