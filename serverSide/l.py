from flask import Flask, render_template, request
from wtforms import HiddenField, TextField
from flask.ext.wtf import Form
app = Flask(__name__)

import mycoinbase



class EmailForm(Form):
    emailAddress = TextField('emailAddress')
    tokenId = HiddenField('tokenId')
    amount = HiddenField('amount')


@app.route('/<tokenId>/<amount>')
def inital_visit(tokenId, amount):
    form = EmailForm(csrf_enabled=False)
    form.amountValue = amount
    form.tokenId.default = tokenId
    form.amount.default = amount
    form.process()
    return render_template('initial.html', title="Withdraw money", form=form)

@app.route('/withdraw', methods=['POST'])
def withdrawal_page():
    tokenId = request.form.get('tokenId')
    emailAddress = request.form.get('emailAddress')
    print tokenId
    print emailAddress
    success = mycoinbase.transferMoney(tokenId, emailAddress)
    if success:
        return render_template('final_success.html', title="Withdraw money")
    else:
        return render_template('final_failed.html', title="Withdraw money")


if __name__ == '__main__':
    """docstring for __main__"""
    app.run(debug=True, host='0.0.0.0', port=80)
    
